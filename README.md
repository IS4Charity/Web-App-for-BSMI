Pengantar:
==========
Rencana pengembangan aplikasi sistem informasi untuk kegiatan charity yang dapat digunakan umum. Project ini untuk kebutuhan BSMI. Hasil pengembangan terbuka dan dapat digunakan bersama untuk kebutuhan sosial non-profit.
Informasi lebih jelas dapat melihat ke halaman wiki di: 
https://gitlab.com/IS4Charity/Web-App-for-BSMI/wikis/home 
 
Installasi:
===========
 - git
 - mysql
 - Erlang/OTP : **Gunakan versi 17**. Versi yang lebih baru (18) tidak support deps riak.
 
 
Running/Deploy:
===============
* Pastikan terhubung ke internet dan dapat akses ke github
* (setelah clone repository) bersihkan isi sub directory deps. Directory tersebut akan lengkapi lagi oleh tools rebar. 
* untuk menjalankan server:
   * Pada windows jalankan perintah: `start-server.bat` 
   * Pada Unix environment jalankan `./rebar get-deps compile` kemudian `./init-dev.sh` 
* Untuk akses ke database. Sesuaikan konfigurasi akses ke database pada berkas 'boss.config'. Pastikan server mysql sudah berjalan dengan konfigurasi database yang sesuai.


Ingin turut berkontribusi?
==========================
* Project management, diskusi dan daftar rencana kerja dapat dilihat di:
https://app.asana.com/0/81276477984571/list
* Daftar API yang akan dikerjakan dapat dilihat di:
http://docs.bsmi.apiary.io/#


Disusun bersama oleh: 

rse.cs.ui.ac.id